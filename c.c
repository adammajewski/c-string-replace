/*

description : 
https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/subwake
https://en.wikipedia.org/wiki/Help:Displaying_a_formula#Color


------------ git --------------------
https://gitlab.com/adammajewski/c-string-replaceing

cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/c-string-replace.git
git add .
git commit
git push -u origin master

-------------- asprintf --------------------------------------
Using asprintf instead of sprintf or snprintf by james
http://www.stev.org/post/2012/02/10/Using-saprintf-instead-of-sprintf-or-snprintf.aspx

http://ubuntuforums.org/showthread.php?t=279801


gcc c.c -D_GNU_SOURCE -Wall // without #define _GNU_SOURCE

gcc c.c -Wall
----------- run ----------------------

./a.out



*/


#define _GNU_SOURCE // asprintf
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // strlen
 
int main() {
    // output = theta+(p/q, r/s) or theta-(p/q, r/s)
    char *sOut  = ""; // in plaint text format
    char *sOutf = ""; // formatted for wikipedia math formula 
    // input
    char *sIn = "00000000000000010";  // rs+ or rs-
    
    // strings which will replace 0 and 1 digit in the input 
    // length(sR0) = length(sR1)
    char *sR0 = "11101"; // pq-
    char *sR1 = "11110"; // pq+


   int iMax; // length of the input string
   int i; 


   iMax = (int) strlen(sIn);

   for (i=0; i<iMax; i++){

   // printf("i = %d, sIn[i] = %c = %d\n", i, sIn[i],  sIn[i] - '0');
    // create sOut by replaceing digit (0 or 1) from aIn by a block of digits :  sR0 or sR1
    if (((int) sIn[i] -'0') == 0 ) // http://stackoverflow.com/questions/868496/how-to-convert-char-to-integer-in-c
       {asprintf(&sOut, "%s%s", sOut, sR0);
       asprintf(&sOutf, "%s\\ {\\color{Blue}%s}", sOutf, sR0); }
       
       else // if sIn[i]==1 
       {asprintf(&sOut, "%s%s", sOut, sR1);
       asprintf(&sOutf, "%s\\ {\\color{Red}%s}", sOutf, sR1);}
   }
      


    printf("input string = sIn = 0.(%s)\n", sIn);
    printf("Input Length = %d\n\n", iMax);
    
    
    printf("replace string for digit 0 = sR0 = 0.(%s)\n", sR0);
    printf("Length of sR0 = %d\n\n", (int) strlen(sR0));
    
    printf("replace string for digit 1 = sR1 = 0.(%s)\n", sR1);
    printf("Length of sR1 = %d\n", (int) strlen(sR1));
    
    
    
    printf("output string in plain form sOut = 0.(%s)\n", sOut);
    printf("Output Length = %d\n\n",  (int) strlen(sOut));
    printf("output string in wikipedia math formula form = sOutf = \n 0.(%s)\n", sOutf);
    printf("sR0 displayed as a blue and sR1 as a red font\n");

    free(sOut); 
    free(sOutf); 
    return 0;
}

