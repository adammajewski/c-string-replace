Replace char from the string by another string. 

Example : 




# description 
* [example of use: subwake in wikibooks](https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/subwake)
* [color in wiki math formula](https://en.wikipedia.org/wiki/Help:Displaying_a_formula#Color)



# Key words
* discrete local complex dynamics
* complex quadratic polynomial
* string



# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* [KaTeX](https://khan.github.io/KaTeX/) to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html). [Here is used version](https://github.com/gitlabhq/gitlabhq/blob/a0715f079c143a362a7f6157db45020b8432003e/vendor/assets/javascripts/katex.js)


## git ( gitlab)

```git
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/c-string-replace.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

Local repo:  ~/c/string/replace

